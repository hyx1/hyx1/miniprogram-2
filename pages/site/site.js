// pages/site/site.js
Page({

 /**
  * 页面的初始数据
  */
 data: {
  communicationList: [
   {
    title: 'A',
    list: [{
     url: '../../images/1.jpg',
     name: '张三',
     age: 19
    }, {
     url: '../../images/2.jpg',
     name: '李四',
     age: 19
    },
    {
     url: '../../images/2.jpg',
     name: '李四',
     age: 19
    }
   ]
   },
   {
    title: 'B',
    list: [{
     url: '../../images/1.jpg',
     name: '张三',
     age: 19
    }, {
     url: '../../images/2.jpg',
     name: '李四',
     age: 19
    }]
   },
   {
    title: 'C',
    list: [{
     url: '../../images/1.jpg',
     name: '张三',
     age: 19
    }, {
     url: '../../images/2.jpg',
     name: '李四',
     age: 19
    }]
   },
   {
    title: 'D',
    list: [{
     url: '../../images/1.jpg',
     name: '张三',
     age: 19
    }, {
     url: '../../images/2.jpg',
     name: '李四',
     age: 19
    }]
   },
   {
    title: 'E',
    list: [{
     url: '../../images/1.jpg',
     name: '张三',
     age: 19
    }, {
     url: '../../images/2.jpg',
     name: '赵武',
     age: 20
    }]
   },
   {
    title: 'F',
    list: [{
     url: '../../images/1.jpg',
     name: '张三',
     age: 19
    }, {
     url: '../../images/2.jpg',
     name: '李四',
     age: 19
    }]
   }, {
    title: 'R',
    list: [{
     url: '../../images/1.jpg',
     name: '张三',
     age: 19
    }, {
     url: '../../images/2.jpg',
     name: '李四',
     age: 19
    }]
   }
  ]

 },
 toview:'',
 clicktitle:function(e){
  console.log(e);
  var letter = e.currentTarget.dataset.index
  console.log(letter)
  this.setData({
   toview:letter
  })
 },

 /**
  * 生命周期函数--监听页面加载
  */
 onLoad: function (options) {
  console.log("options", options)
 },

 /**
  * 生命周期函数--监听页面初次渲染完成
  */
 onReady: function () {

 },

 /**
  * 生命周期函数--监听页面显示
  */
 onShow: function () {

 },

 /**
  * 生命周期函数--监听页面隐藏
  */
 onHide: function () {

 },

 /**
  * 生命周期函数--监听页面卸载
  */
 onUnload: function () {

 },

 /**
  * 页面相关事件处理函数--监听用户下拉动作
  */
 onPullDownRefresh: function () {

 },

 /**
  * 页面上拉触底事件的处理函数
  */
 onReachBottom: function () {

 },

 /**
  * 用户点击右上角分享
  */
 onShareAppMessage: function () {

 }
})