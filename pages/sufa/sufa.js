// pages/sufa/sufa.js
Page({

 /**
  * 页面的初始数据
  */
 data: {
  // studentDetails:[
  //      {
  //        id:1,
  //        gender:'male',
  //        img:'../../images/tx1.jpg',
  //        name:'郝禹锡',
  //        wechatname:'cx330',
  //        address:'内蒙古 巴彦淖尔',
  //        circle:[
  //          '../../img/1.png','../../img/1.png','../../img/1.png'
  //        ]
  //      },
  //      {
  //        id:2,
  //        gender:'female',
  //        img:'../../img/1.png',
  //        name:'刘嘉伟',
  //        wechatname:'jarvis',
  //        address:'江苏无锡',
  //        circle:[
  //          '../../img/1.png','../../img/1.png','../../img/1.png'
  //        ]
  //      }
  //    ],
 },

 /**
  * 生命周期函数--监听页面加载
  */
 onLoad: function (options) {
  console.log("options",options)
  var index=options.index;
  var list=getApp().studentDetails;
  console.log(list)
  var student=list[index];
  console.log(student)
  this.setData({
   student:student
  })
 },

 /**
  * 生命周期函数--监听页面初次渲染完成
  */
 onReady: function () {

 },

 /**
  * 生命周期函数--监听页面显示
  */
 onShow: function () {

 },

 /**
  * 生命周期函数--监听页面隐藏
  */
 onHide: function () {

 },

 /**
  * 生命周期函数--监听页面卸载
  */
 onUnload: function () {

 },

 /**
  * 页面相关事件处理函数--监听用户下拉动作
  */
 onPullDownRefresh: function () {

 },

 /**
  * 页面上拉触底事件的处理函数
  */
 onReachBottom: function () {

 },

 /**
  * 用户点击右上角分享
  */
 onShareAppMessage: function () {

 }
})